
CREATE TABLE [Admin]
( 
	[IdAdm]              int  NOT NULL 
)
go

CREATE TABLE [Jelo]
( 
	[IdJelo]             int  NOT NULL ,
	[Sastojci]           varchar(20)  NOT NULL ,
	[Naziv]              varchar(20)  NOT NULL ,
	[Cena]               decimal(5,2)  NOT NULL ,
	[Slika]              varchar(20)  NOT NULL ,
	[IdRes]              int  NOT NULL 
)
go

CREATE TABLE [Komentar]
( 
	[IdKom]              char(18)  NOT NULL ,
	[Komentar]           char(18)  NULL ,
	[Ocena]              char(18)  NULL ,
	[IdKor]              int  NOT NULL ,
	[IdRes]              int  NOT NULL 
)
go

CREATE TABLE [Korisnik]
( 
	[IdKor]              int  NOT NULL ,
	[KorIme]             varchar(20)  NOT NULL ,
	[Lozinka]            varchar(20)  NOT NULL 
)
go

CREATE TABLE [Korpa]
( 
	[IdKor]              int  NOT NULL ,
	[Kolicina]           integer  NOT NULL ,
	[IdJelo]             int  NOT NULL 
)
go

CREATE TABLE [Narudzbina]
( 
	[UkupanIznos]        char(18)  NULL ,
	[IdNar]              char(18)  NOT NULL ,
	[IdKor]              int  NOT NULL ,
	[IdJelo]             int  NOT NULL 
)
go

CREATE TABLE [RegKorisnik]
( 
	[IdKor]              int  NOT NULL ,
	[Adresa]             varchar(20)  NOT NULL ,
	[Ime]                varchar(20)  NOT NULL ,
	[Prezime]            varchar(20)  NOT NULL ,
	[Mejl]               varchar(20)  NOT NULL ,
	[BrTelefona]         varchar(20)  NOT NULL 
)
go

CREATE TABLE [Restoran]
( 
	[IdRes]              int  NOT NULL ,
	[Adresa]             varchar(20)  NOT NULL ,
	[Ime]                integer  NOT NULL ,
	[Mejl]               varchar(20)  NOT NULL ,
	[BrTelefona]         varchar(7)  NOT NULL ,
	[TipHrane]           varchar(20)  NOT NULL ,
	[KratakOpis]         varchar(20)  NULL ,
	[Slika]              varchar(20)  NULL 
)
go

ALTER TABLE [Admin]
	ADD CONSTRAINT [XPKAdmin] PRIMARY KEY  CLUSTERED ([IdAdm] ASC)
go

ALTER TABLE [Jelo]
	ADD CONSTRAINT [XPKJelo] PRIMARY KEY  CLUSTERED ([IdJelo] ASC)
go

ALTER TABLE [Komentar]
	ADD CONSTRAINT [XPKKomentar] PRIMARY KEY  CLUSTERED ([IdKom] ASC)
go

ALTER TABLE [Korisnik]
	ADD CONSTRAINT [XPKKorisnik] PRIMARY KEY  CLUSTERED ([IdKor] ASC)
go

ALTER TABLE [Korisnik]
	ADD CONSTRAINT [KorImeKey] UNIQUE ([KorIme]  ASC)
go

ALTER TABLE [Korisnik]
	ADD CONSTRAINT [Lozinka] UNIQUE ([Lozinka]  ASC,[KorIme]  ASC)
go

ALTER TABLE [Korpa]
	ADD CONSTRAINT [XPKKorpa] PRIMARY KEY  CLUSTERED ([IdKor] ASC,[IdJelo] ASC)
go

ALTER TABLE [Narudzbina]
	ADD CONSTRAINT [XPKNarudzbina] PRIMARY KEY  CLUSTERED ([IdNar] ASC,[IdKor] ASC,[IdJelo] ASC)
go

ALTER TABLE [RegKorisnik]
	ADD CONSTRAINT [XPKRegKorisnik] PRIMARY KEY  CLUSTERED ([IdKor] ASC)
go

ALTER TABLE [RegKorisnik]
	ADD CONSTRAINT [KeyMejl] UNIQUE ([Mejl]  ASC)
go

ALTER TABLE [Restoran]
	ADD CONSTRAINT [XPKRestoran] PRIMARY KEY  CLUSTERED ([IdRes] ASC)
go

ALTER TABLE [Restoran]
	ADD CONSTRAINT [XAK1Mejl] UNIQUE ([Mejl]  ASC)
go


ALTER TABLE [Admin]
	ADD CONSTRAINT [R_1] FOREIGN KEY ([IdAdm]) REFERENCES [Korisnik]([IdKor])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [Jelo]
	ADD CONSTRAINT [R_8] FOREIGN KEY ([IdRes]) REFERENCES [Restoran]([IdRes])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Komentar]
	ADD CONSTRAINT [R_6] FOREIGN KEY ([IdKor]) REFERENCES [RegKorisnik]([IdKor])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [Komentar]
	ADD CONSTRAINT [R_7] FOREIGN KEY ([IdRes]) REFERENCES [Restoran]([IdRes])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Korpa]
	ADD CONSTRAINT [R_9] FOREIGN KEY ([IdKor]) REFERENCES [RegKorisnik]([IdKor])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [Korpa]
	ADD CONSTRAINT [R_10] FOREIGN KEY ([IdJelo]) REFERENCES [Jelo]([IdJelo])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Narudzbina]
	ADD CONSTRAINT [R_11] FOREIGN KEY ([IdKor],[IdJelo]) REFERENCES [Korpa]([IdKor],[IdJelo])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [RegKorisnik]
	ADD CONSTRAINT [R_4] FOREIGN KEY ([IdKor]) REFERENCES [Korisnik]([IdKor])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [Restoran]
	ADD CONSTRAINT [R_5] FOREIGN KEY ([IdRes]) REFERENCES [Korisnik]([IdKor])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go
